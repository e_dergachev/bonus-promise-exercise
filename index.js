const fs = require('fs');

( async () => {
    const pr = (file, encoding) => {
        return new Promise((resolve, reject) => {
            fs.readFile(file, encoding, (err, data) => err ? reject(err) : resolve(data));
        });
    };
    try {
        let data = await pr('package.json', 'utf-8');
        console.log(data);
    }
    catch (err) {
        console.log(err);
    }    
    try {
        let data = await fs.promises.readFile('package.json', 'utf-8');
        console.log(data);
    }
    catch (err) {
        console.log(err);
    }
})();
